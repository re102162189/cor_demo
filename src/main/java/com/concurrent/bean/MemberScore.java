package com.concurrent.bean;

import java.util.Map;

public class MemberScore {
	
	public enum Score {
		METHOD1, METHOD2, METHOD3
	}
	
	private long userId;
	private Map<Score, Integer> scores;
	private int sum;

	public MemberScore(long userId) {
		this.userId = userId;
	}

	public long getUserId() {
		return userId;
	}

	public Map<Score, Integer> getScores() {
		return scores;
	}

	public void setScores(Map<Score, Integer> scores) {
		this.scores = scores;
	}

	public int getSum() {
		return sum;
	}

	public void setSum(int sum) {
		this.sum = sum;
	}

	@Override
	public String toString() {
		return "MemberScore [userId=" + userId + ", scores=" + scores + ", sum=" + sum + "]";
	}
}
