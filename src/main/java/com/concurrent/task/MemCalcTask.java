package com.concurrent.task;

import java.util.concurrent.Callable;

import com.concurrent.bean.MemberScore;
import com.concurrent.handler.MemCalcChain;

public class MemCalcTask implements Callable<MemberScore> {
	private final MemCalcChain memCalcChain;
	private final MemberScore memberScore;

	public MemCalcTask(MemberScore memberScore, MemCalcChain memCalcChain) {
	    this.memCalcChain = memCalcChain;
	    this.memberScore = memberScore;
	  }

	@Override
	public MemberScore call() throws Exception {
		memCalcChain.doCalculate(memberScore);
		return memberScore;
	}
}
