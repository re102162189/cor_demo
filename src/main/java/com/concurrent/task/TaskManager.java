package com.concurrent.task;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.concurrent.bean.MemberScore;
import com.concurrent.handler.MemCalcChain;
import com.concurrent.handler.Method1Handler;
import com.concurrent.handler.Method2Handler;
import com.concurrent.handler.Method3Handler;

public class TaskManager {

	private static final int MAX_INGEST = 10;
	private final ExecutorService executorService = Executors.newFixedThreadPool(MAX_INGEST);

	// I mock map as DB here
	private Map<Long, MemberScore> results = new HashMap<>();

	// I mock 5000 users
	private Queue<Integer> users = new LinkedList<>();

	public static void main(String[] args) throws InterruptedException {
		TaskManager taskManager = new TaskManager();
		taskManager.processScore();
	}

	private void processScore() throws InterruptedException {
		Set<Callable<MemberScore>> callables = new HashSet<>();
		
		// query users from DB here
		for (int i = 1; i <= 5000; i++) {
			users.add(i);
		}

		while (!users.isEmpty()) {
			MemCalcChain memCalcChain = new MemCalcChain(new Method1Handler(), new Method2Handler(),
					new Method3Handler());
			MemberScore memberScore = new MemberScore(users.poll());
			memberScore.setScores(new HashMap<>());
			MemCalcTask memCalcTask = new MemCalcTask(memberScore, memCalcChain);
			callables.add(memCalcTask);

			if (callables.size() == MAX_INGEST) {
				invokeTasks(callables);
			}
		}

		if (callables.size() > 0) {
			invokeTasks(callables);
		}

		// assert here, add '-ea' in JVM arguments
		assert results.size()     == 4997 : "result size is 4997";
		assert results.get(   1L) == null : "userid    1 is null";
		assert results.get( 100L) == null : "userid  100 is null";
		assert results.get(1000L) == null : "userid 1000 is null";

		assert results.get(   5L).getSum() ==    21 : "userid    5 score is 13506";
		assert results.get(  50L).getSum() ==   156 : "userid   50 score is 13506";
		assert results.get( 500L).getSum() ==  1506 : "userid  500 score is 13506";
		assert results.get(4500L).getSum() == 13506 : "userid 4500 score is 13506";
		
		// debug details
		System.out.println("user id: " + 5 + " score: " + results.get(5L));
		System.out.println("user id: " + 50 + " score: " + results.get(50L));
		System.out.println("user id: " + 500 + " score: " + results.get(500L));
		System.out.println("user id: " + 4500 + " score: " + results.get(4500L));
		
		// DO close the executorService at the end
		executorService.shutdown();
	}

	private void invokeTasks(Set<Callable<MemberScore>> callables) throws InterruptedException {
		List<Future<MemberScore>> futures = executorService.invokeAll(callables);
		Map<Long, MemberScore> tempResults = new HashMap<>();
		for (Future<MemberScore> future : futures) {
			try {
				// batch insert scores to DB here
				MemberScore result = future.get();
				tempResults.put(result.getUserId(), result);

				// mock DB here, flush results when reach MAX_INGEST
				if (tempResults.size() == MAX_INGEST) {
					results.putAll(tempResults);
					tempResults.clear();
				}
			} catch (Exception e) {
				// get any calculation error will drop the chain
				System.err.println(e);
			}
		}

		// mock DB here, flush results when there's any left
		if (tempResults.size() > 0) {
			results.putAll(tempResults);
			tempResults.clear();
		}

		callables.clear();
	}
}
