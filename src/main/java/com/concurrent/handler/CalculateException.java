package com.concurrent.handler;

public class CalculateException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public CalculateException(String errorMessage) {
        super(errorMessage);
    }
}
