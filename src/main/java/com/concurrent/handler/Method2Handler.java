package com.concurrent.handler;

import com.concurrent.bean.MemberScore;
import com.concurrent.bean.MemberScore.Score;

public class Method2Handler implements IMemCalcHandler {

	@Override
	public void doCalculate(MemberScore memberScore, MemCalcChain memCalcChain) throws CalculateException {
		// TODO
		// =======================
		if (memberScore.getUserId() == 100) {
			throw new CalculateException("something wrong in method2 for userid 100");
		}
		int seed = (int)memberScore.getUserId();
		memberScore.getScores().put(Score.METHOD2, seed + 2);
		// =======================
		memCalcChain.doCalculate(memberScore);
	}

}
