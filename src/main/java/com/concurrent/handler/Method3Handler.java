package com.concurrent.handler;

import com.concurrent.bean.MemberScore;
import com.concurrent.bean.MemberScore.Score;

public class Method3Handler implements IMemCalcHandler {

	@Override
	public void doCalculate(MemberScore memberScore, MemCalcChain memCalcChain) throws CalculateException {
		// TODO
		// =======================
		if (memberScore.getUserId() == 1000) {
			throw new CalculateException("something wrong in method3 for userid 1000");
		}
		int seed = (int)memberScore.getUserId();
		memberScore.getScores().put(Score.METHOD3, seed + 3);
		// =======================
		memCalcChain.doCalculate(memberScore);
	}

}
