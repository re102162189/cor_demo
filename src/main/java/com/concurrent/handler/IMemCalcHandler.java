package com.concurrent.handler;

import com.concurrent.bean.MemberScore;

public interface IMemCalcHandler {	
	void doCalculate(MemberScore memberScore, MemCalcChain memCalcChain) throws CalculateException;
}
