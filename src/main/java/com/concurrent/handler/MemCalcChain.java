package com.concurrent.handler;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

import com.concurrent.bean.MemberScore;
import com.concurrent.bean.MemberScore.Score;

public class MemCalcChain {

	private Iterator<IMemCalcHandler> handlers;

	public MemCalcChain(IMemCalcHandler... handlers) {
		this.handlers = Arrays.asList(handlers).iterator();
	}

	public void doCalculate(MemberScore memberScore) throws CalculateException {
		if (handlers.hasNext()) {
			IMemCalcHandler handler = handlers.next();
			handler.doCalculate(memberScore, this);
		}

		int sum = 0;
		for(Map.Entry<Score, Integer> scores: memberScore.getScores().entrySet()) {
			sum += scores.getValue();
		}
		memberScore.setSum(sum);
	}
}
