package com.concurrent.handler;

import com.concurrent.bean.MemberScore;
import com.concurrent.bean.MemberScore.Score;

public class Method1Handler implements IMemCalcHandler {

	@Override
	public void doCalculate(MemberScore memberScore, MemCalcChain memCalcChain) throws CalculateException {
		// TODO
		// =======================
		if (memberScore.getUserId() == 1) {
			throw new CalculateException("something wrong in method1 for userid 1");
		}
		int seed = (int) memberScore.getUserId();
		memberScore.getScores().put(Score.METHOD1, seed + 1);
		// =======================
		memCalcChain.doCalculate(memberScore);
	}

}
